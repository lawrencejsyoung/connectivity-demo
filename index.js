const minimist = require('minimist')
const hotels = require('./src/commands/hotels')

module.exports = () => {
  const args = minimist(process.argv.slice(2))
  const cmd = args._[0]

  switch (cmd) {
    case 'hotels':
      hotels(args)
      break

    default:
      console.error(`"${cmd}" is not a valid command!`)
      break
  }
}