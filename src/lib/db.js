module.exports = require('knex') ({
    client: 'postgres',
    connection: {
      host: 'localhost',
      user: 'postgres',
      password: 'postgres',
      database: 'postgres'
    },
    ssl: true,
    pool: {
      min: 0,
      max: 10,
    },
});