const axios = require('axios')

const getHotels = require('../services/get_hotels')
const getLegacyHotels = require('../services/get_legacy_hotels')
const insertHotel = require('../services/insert_hotel')

module.exports = async (args) => {
  try {
    const city = args.city
    const checkin = args.checkin
    const checkout = args.checkout

    let hotelList;

    await axios.all([getHotels(city, checkin, checkout), getLegacyHotels(city, checkin, checkout)])
      .then(axios.spread(function (hotels, legacyHotels) {
        hotelList = removeDuplicates(hotels, legacyHotels)
        console.log(hotelList)
      }));
    
    for (let hotel of hotelList) {
      await insertHotel(hotel)
    }

    console.log(`Updated hotel data`)

  } catch (err) {
    console.error(err)
  }
}

const removeDuplicates = (hotels, legacyHotels) => {
  let hotelList = [];
  for (let hotel of hotels.hotels) {
    for (let legacyHotel of legacyHotels.root.element) {
      if (hotel.name === legacyHotel.name) {
        let newHotel = {...hotel}
        delete newHotel.price
        newHotel.prices = {
          snaptravel: hotel.price,
          retail: legacyHotel.price
        }
        hotelList.push(newHotel)
      }
    }
  }

  return hotelList
}
