const axios = require('axios')

module.exports = async (city_string_input, checkin_string_input, checkout_string_output) => {
  try { 
    const results = await axios({
      method: 'post',
      url: 'https://experimentation.getsnaptravel.com/interview/hotels',
      data: {
        city: city_string_input,
        checkin: checkin_string_input,
        checkout: checkout_string_output,
        provider: 'snaptravel'
      }
    })

    return results.data
  } catch (err) {
    console.error(err.message)
  }
 }