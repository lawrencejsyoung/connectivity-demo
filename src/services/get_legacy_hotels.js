const axios = require('axios')
const parser = require('fast-xml-parser')
const he = require('he')

module.exports = async (city_string_input, checkin_string_input, checkout_string_output) => {
  try {
    const xmlData = 
    `<?xml version="1.0" encoding="UTF-8"?>
    <root>
      <checkin>${checkin_string_input}</checkin>
      <checkout>${checkout_string_output}</checkout>
      <city>${city_string_input}</city>
      <provider>snaptravel</provider>
    </root>  
    `

    const results = await axios({
      method: 'post',
      url: 'https://experimentation.getsnaptravel.com/interview/legacy_hotels',
      headers: {
        'Content-Type': 'text/xml'
      },
      data: xmlData
    })
    
    const parsedResultsData = parseHotels(results.data)

    return parsedResultsData
  } catch (err) {
    console.error(err.message)
  } 
}

const parseHotels = (xmlData) => {
  const options = {
    attributeNamePrefix : "@_",
    attrNodeName: "attr", //default is 'false'
    textNodeName : "#text",
    ignoreAttributes : true,
    ignoreNameSpace : false,
    allowBooleanAttributes : false,
    parseNodeValue : true,
    parseAttributeValue : false,
    trimValues: true,
    cdataTagName: "__cdata", //default is 'false'
    cdataPositionChar: "\\c",
    localeRange: "", //To support non english character in tag/attribute values.
    parseTrueNumberOnly: false,
    attrValueProcessor: a => he.decode(a, {isAttributeValue: true}),//default is a=>a
    tagValueProcessor : a => he.decode(a) //default is a=>a
  }
  
  let jsonObj

  if ( parser.validate(xmlData) === true) { //optional (it'll return an object in case it's not valid)
    jsonObj = parser.parse(xmlData,options)
  }

  return jsonObj
}