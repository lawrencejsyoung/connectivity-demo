const db = require('../lib/db')

module.exports = async (hotel) => {
  try {
    await db('hotels').insert({
      hotel: JSON.stringify(hotel)
    })
  } catch (err) {
    console.error(err)
  }
}